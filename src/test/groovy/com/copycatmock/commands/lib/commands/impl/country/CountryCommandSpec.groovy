package com.copycatmock.commands.lib.commands.impl.country

import com.copycatmock.commands.lib.commands.impl.FormatCommand
import com.copycatmock.commands.lib.commands.impl.faker.FakerCommand
import com.github.javafaker.Faker
import spock.lang.Specification

class CountryCommandSpec extends Specification {

    private static final String ZONE_ID = "America/Sao_Paulo";
    private static final String LOCALE_NAME = "pt_BR";

    FormatCommand formatCmdMock
    FakerCommand fakerCmdMock

    CountryCommand countryCmd

    def "setup"() {
        formatCmdMock = Mock(FormatCommand)
        fakerCmdMock = Mock(FakerCommand)

        countryCmd = new CountryCommandStub(formatCmdMock, fakerCmdMock)
    }


    def "should return current Brazilian LocalDateTime formatted"() {
        given:
        String format = "YYYY-MM-dd'T'HH:mm:ss'Z'"
        String expectedResult = "2020-09-23T13:54:00Z"

        when:
        String result = countryCmd.now(format)

        then:
        1 * formatCmdMock.now(format, ZONE_ID) >> expectedResult
        assert result == expectedResult
    }

    def "should return Brazilian currency formatted"() {
        given:
        String moneyValue = 1.23
        String expectedResult = 'R$ 1,23'

        when:
        String result = countryCmd.money(moneyValue)

        then:
        1 * formatCmdMock.money(moneyValue, LOCALE_NAME) >> expectedResult
        assert result == expectedResult
    }

    def "should return faker instance with Locale"() {
        given:
        Faker fakerMock = Mock(Faker)

        when:
        Faker result = countryCmd.faker()

        then:
        1 * fakerCmdMock.withLocale(LOCALE_NAME) >> fakerMock
        assert result == fakerMock
    }

    private class CountryCommandStub extends CountryCommand {

        CountryCommandStub(FormatCommand formatCmd, FakerCommand fakerCmd) {
            super(formatCmd, fakerCmd)
        }

        @Override
        String getZoneId() {
            return ZONE_ID
        }

        @Override
        String getLocaleName() {
            return LOCALE_NAME
        }

        @Override
        String getCommandKey() {
            return "test"
        }
    }
}

package com.copycatmock.commands.lib.commands.impl.country.br.components

import spock.lang.Specification

class CNPJSpec extends Specification {

    def "should transform CNPJ to String"() {
        given:
        CNPJ cnpj = new CNPJ()

        when:
        String result = cnpj.toString()

        then:
        assert result.matches("(^\\d{14}\$)")
    }

    def "should format CNPJ"() {
        given:
        CNPJ cnpj = new CNPJ()

        when:
        String result = cnpj.format()

        then:
        assert result.matches("(^\\d{2}.\\d{3}.\\d{3}/\\d{4}-\\d{2}\$)")
    }
}

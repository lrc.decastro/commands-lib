package com.copycatmock.commands.lib.commands.impl.country.br

import com.copycatmock.commands.lib.commands.impl.FormatCommand
import com.copycatmock.commands.lib.commands.impl.country.br.components.CNPJ
import com.copycatmock.commands.lib.commands.impl.country.br.components.CPF
import com.copycatmock.commands.lib.commands.impl.faker.FakerCommand
import spock.lang.Specification

class BrCommandSpec extends Specification {

    FormatCommand formatCmdMock
    FakerCommand fakerCmdMock

    BrCommand brCmd

    def "setup"() {
        formatCmdMock = Mock(FormatCommand)
        fakerCmdMock = Mock(FakerCommand)

        brCmd = new BrCommand(formatCmdMock, fakerCmdMock)
    }

    def "should generate CPF"() {
        when:
        CPF cpf = brCmd.cpf()

        then:
        assert cpf != null
    }

    def "should generate CNPJ"() {
        when:
        CNPJ cnpj = brCmd.cnpj()

        then:
        assert cnpj != null
    }
}

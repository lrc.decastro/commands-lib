package com.copycatmock.commands.lib.commands.impl.country.br.components

import spock.lang.Specification

class CPFSpec extends Specification {

    def "should transform CPF to String"() {
        given:
        CPF cpf = new CPF()

        when:
        String result = cpf.toString()

        then:
        assert result.matches("(^\\d{11}\$)")
    }

    def "should format CPF"() {
        given:
        CPF cpf = new CPF()

        when:
        String result = cpf.format()

        then:
        assert result.matches("(^\\d{3}\\.\\d{3}\\.\\d{3}-\\d{2}\$)")
    }
}

package com.copycatmock.commands.lib.commands.impl.faker

import com.github.javafaker.Faker
import spock.lang.Specification

class FakerWrapperSpec extends Specification {

    FakerCommand fakerWrapper

    def "setup"() {
        fakerWrapper = new FakerCommand()
    }

    def "should setup faker with locale"() {
        given:
        String localeName = "it_IT"

        when:
        Faker result = fakerWrapper.withLocale(localeName)

        then:
        assert result != null
    }
}

package com.copycatmock.commands.lib.commands.impl


import spock.lang.Specification
import spock.lang.Unroll

class ConvertCommandSpec extends Specification {

    ConvertCommand converter

    def "setup"() {
        converter = new ConvertCommand()
    }

    @Unroll
    def "should convert #methodName"() {
        when:
        Object result = converter."$methodName"(value)
        Class resultClass = result != null ? result.class : null

        then:
        assert result == expectedResult
        assert resultClass == expectedClass

        where:
        methodName     | value | expectedResult | expectedClass
        "toInteger"    | 1     | 1              | Integer
        "toInteger"    | "0"   | 0              | Integer
        "toInteger"    | "1"   | 1              | Integer
        "toInteger"    | "-1"  | -1             | Integer
        "toInteger"    | null  | null           | null

        "toLong"       | 1     | 1              | Long
        "toLong"       | "0"   | 0              | Long
        "toLong"       | "1"   | 1              | Long
        "toLong"       | "-1"  | -1             | Long
        "toLong"       | null  | null           | null

        "toFloat"      | 1     | 1              | Float
        "toFloat"      | "0"   | 0              | Float
        "toFloat"      | "1"   | 1              | Float
        "toFloat"      | "-1"  | -1             | Float
        "toFloat"      | null  | null           | null

        "toDouble"     | 1     | 1              | Double
        "toDouble"     | "0"   | 0              | Double
        "toDouble"     | "1"   | 1              | Double
        "toDouble"     | "-1"  | -1             | Double
        "toDouble"     | null  | null           | null

        "toBigInteger" | 1     | 1              | BigInteger
        "toBigInteger" | "0"   | 0              | BigInteger
        "toBigInteger" | "1"   | 1              | BigInteger
        "toBigInteger" | "-1"  | -1             | BigInteger
        "toBigInteger" | null  | null           | null

        "toBigDecimal" | 1     | 1              | BigDecimal
        "toBigDecimal" | "0"   | 0              | BigDecimal
        "toBigDecimal" | "1"   | 1              | BigDecimal
        "toBigDecimal" | "-1"  | -1             | BigDecimal
        "toBigDecimal" | null  | null           | null

        "toString"     | 1     | "1"            | String
        "toString"     | 0     | "0"            | String
        "toString"     | "1"   | "1"            | String
        "toString"     | -1    | "-1"           | String
        "toString"     | null  | null           | null
    }
}

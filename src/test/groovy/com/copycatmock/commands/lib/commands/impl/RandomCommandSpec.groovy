package com.copycatmock.commands.lib.commands.impl


import spock.lang.Specification
import spock.lang.Unroll

import java.text.DecimalFormat
import java.text.NumberFormat

class RandomCommandSpec extends Specification {

    Random randomSpy
    RandomCommand randomGenerator

    NumberFormat numberFormat = new DecimalFormat("#")

    def "setup"() {
        randomSpy = Spy(Random)
        randomGenerator = new RandomCommand(randomSpy)
    }

    @Unroll
    def "should generate #methodName"() {
        when:
        Number result = randomGenerator."$methodName"()

        then:
        1 * randomSpy."$methodName"()
        assert result.class == expectedClass
        assert result > 0

        where:
        methodName   | expectedClass
        "nextInt"    | Integer
        "nextLong"   | Long
        "nextFloat"  | Float
        "nextDouble" | Double
    }

    @Unroll
    def "should generate #methodName - input length #length will generate output with length #expectedLength"() {
        when:
        Number result = randomGenerator."$methodName"(length)

        then:
        1 * randomSpy.nextDouble()
        assert numberFormat.format(result).length() == expectedLength
        assert result > 0

        where:
        methodName   | length | expectedLength
        "nextInt"    | -1     | 1
        "nextInt"    | 0      | 1
        "nextInt"    | 1      | 1
        "nextInt"    | 5      | 5
        "nextInt"    | 10     | 10
        "nextInt"    | 19     | 10
        "nextInt"    | 50     | 10
        "nextInt"    | 250    | 10
        "nextInt"    | 1000   | 10

        "nextLong"   | -1     | 1
        "nextLong"   | 0      | 1
        "nextLong"   | 1      | 1
        "nextLong"   | 5      | 5
        "nextLong"   | 10     | 10
        "nextLong"   | 19     | 19
        "nextLong"   | 50     | 19
        "nextLong"   | 250    | 19
        "nextLong"   | 1000   | 19

        "nextFloat"  | -1     | 1
        "nextFloat"  | 0      | 1
        "nextFloat"  | 1      | 1
        "nextFloat"  | 5      | 5
        "nextFloat"  | 10     | 10
        "nextFloat"  | 19     | 19
        "nextFloat"  | 50     | 19
        "nextFloat"  | 250    | 19
        "nextFloat"  | 1000   | 19

        "nextDouble" | -1     | 1
        "nextDouble" | 0      | 1
        "nextDouble" | 1      | 1
        "nextDouble" | 5      | 5
        "nextDouble" | 10     | 10
        "nextDouble" | 19     | 19
        "nextDouble" | 50     | 50
        "nextDouble" | 250    | 50
        "nextDouble" | 1000   | 50
    }

    @Unroll
    def "should generate nextString - input length #length will generate output with length #expectedLength"() {
        when:
        String result = randomGenerator.nextString(length)

        then:
        assert result.length() == expectedLength

        where:
        length | expectedLength
        -1     | 0
        0      | 0
        1      | 1
        5      | 5
        10     | 10
        19     | 19
        50     | 50
        100    | 100
        250    | 100
        1000   | 100
    }


    @Unroll
    def "should generate nextNumericString - input length #length will generate output with length #expectedLength"() {
        when:
        String result = randomGenerator.nextNumericString(length)
        new BigInteger(result)

        then:
        noExceptionThrown()
        assert result.length() == expectedLength


        where:
        length | expectedLength
        -1     | 1
        0      | 1
        1      | 1
        5      | 5
        10     | 10
        19     | 19
        50     | 50
        100    | 100
        250    | 100
        1000   | 100
    }
}

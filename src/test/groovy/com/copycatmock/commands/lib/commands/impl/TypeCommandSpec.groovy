package com.copycatmock.commands.lib.commands.impl

import spock.lang.Specification
import spock.lang.Unroll

class TypeCommandSpec extends Specification {

    TypeCommand typeCommand

    def "setup"() {
        typeCommand = new TypeCommand()
    }

    @Unroll
    def "should check value type by class name"() {

        when:
        boolean result = typeCommand.isInstance(className, value)

        then:
        assert result == expected

        where:
        className          | value | expected
        "java.lang.String" | "abc" | true
        "java.lang.String" | 1     | false
        "java.lang.Object" | "abc" | true
        "java.lang.Object" | 1     | true
    }

    @Unroll
    def "should check value type with method #method - value: #value - expected: #expected"() {

        when:
        boolean result = typeCommand."$method"(value)

        then:
        assert result == expected

        where:
        method         | value              | expected
        "isInteger"    | 1                  | true
        "isInteger"    | "1"                | false
        "isNumber"     | 1                  | true
        "isNumber"     | 1.1                | true
        "isNumber"     | "1"                | false
        "isString"     | 1                  | false
        "isString"     | "1"                | true
        "isBoolean"    | 1                  | false
        "isBoolean"    | "1"                | false
        "isBoolean"    | true               | true
        "isCollection" | 1                  | false
        "isCollection" | "1"                | false
        "isCollection" | ["1"]              | true
        "isCollection" | new HashSet(["1"]) | true
        "isCollection" | [a: "1"]           | false
        "isList"       | 1                  | false
        "isList"       | "1"                | false
        "isList"       | ["1"]              | true
        "isList"       | [a: "1"]           | false
        "isSet"        | 1                  | false
        "isSet"        | "1"                | false
        "isSet"        | ["1"]              | false
        "isSet"        | new HashSet(["1"]) | true
        "isSet"        | [a: "1"]           | false
        "isMap"        | 1                  | false
        "isMap"        | "1"                | false
        "isMap"        | ["1"]              | false
        "isMap"        | new HashSet(["1"]) | false
        "isMap"        | [a: "1"]           | true

    }
}

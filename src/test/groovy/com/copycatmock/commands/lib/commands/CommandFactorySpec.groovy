package com.copycatmock.commands.lib.commands


import com.copycatmock.commands.lib.provided.DatabaseSupplier
import org.reflections.Reflections
import spock.lang.Specification

import java.util.stream.Collectors

class CommandFactorySpec extends Specification {

    private static final String PACKAGE_NAME = Command.getPackage().getName()

    def "should create CommandFactory"() {
        given:
        Set<?> allCommands = getAllCommandClasses()

        DatabaseSupplier dbSupplierMock = Mock(DatabaseSupplier)

        when:
        CommandFactory factory = new CommandFactory(dbSupplierMock)

        then:
        assert factory.getCommands() != null
        assert factory.getCommands().size() == allCommands.size()
    }

    Set<?> getAllCommandClasses() {
        Reflections reflections = new Reflections(PACKAGE_NAME)
        return reflections.getSubTypesOf(Command.class).stream()
                .filter(c -> !c.isAnonymousClass() && !c.isMemberClass())
                .collect(Collectors.toSet())
    }
}

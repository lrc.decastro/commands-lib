package com.copycatmock.commands.lib.commands.impl

import spock.lang.Specification
import spock.lang.Unroll

class ListCommandSpec extends Specification {

    ListCommand listCommand

    def "setup"() {
        listCommand = new ListCommand()
    }

    @Unroll
    def "should check if list is empty"() {
        when:
        boolean result = listCommand.isEmpty(list)

        then:
        assert result == expectedResult

        where:
        list       | expectedResult
        null       | true
        []         | true
        ["1"]      | false
        ["1", "2"] | false
        [null]     | false
    }

    def "should filter list"() {
        when:
        List result = listCommand.filter(list, predicate)

        then:
        assert result == expectedResult

        where:
        list            | predicate         | expectedResult
        ["1", "2", "1"] | { s -> s == "1" } | ["1", "1"]
        [1, 2, 3]       | { v -> v > 1 }    | [2, 3]

    }

}

package com.copycatmock.commands.lib.commands.impl

import com.copycatmock.commands.lib.commands.impl.totp.TotpCommand
import com.eatthepath.otp.TimeBasedOneTimePasswordGenerator
import spock.lang.Specification
import spock.lang.Unroll

import java.time.Clock
import java.time.LocalDate
import java.time.ZoneId
import java.time.ZonedDateTime

class TotpCommandSpec extends Specification {

    private static final String SP_ZONE_ID = "America/Sao_Paulo"
    private static final ZoneId SP_ZONE = ZoneId.of(SP_ZONE_ID)
    private static final ZonedDateTime SP_ZONE_DATE_TIME = LocalDate.of(2020, 9, 23).atTime(13, 54, 1).atZone(SP_ZONE)
    private static final int GENERATED_PASSWORD = 260048
    private static final int DEFAULT_KEY_LENGTH = 160

    TimeBasedOneTimePasswordGenerator totpGeneratorSpy

    TotpCommand totpCommand

    def "setup"() {
        Clock clockStub = Clock.fixed(SP_ZONE_DATE_TIME.toInstant(), SP_ZONE)
        totpGeneratorSpy = new TimeBasedOneTimePasswordGenerator()

        totpCommand = new TotpCommand(clockStub, totpGeneratorSpy, DEFAULT_KEY_LENGTH)
    }

    def "should generate password"() {
        given: "Expected result for fixed clock"
        int expectedResult = GENERATED_PASSWORD

        when:
        int result = totpCommand.generatePassword()

        then:
        assert result == expectedResult
    }

    @Unroll
    def "should validate password - #descr"() {
        when:
        boolean result = totpCommand.validate(password)

        then:
        assert result == expectedResult

        where:
        descr                                                                             | password           | expectedResult
        "Should return true if generates a password that is equals to the provided"       | GENERATED_PASSWORD | true
        "Should return false if generates a password that is different from the provided" | 111111             | false
    }

    def "should generate Key"() {
        when:
        String result = totpCommand.generateKey()

        then:
        assert result != null
    }

    def "should validate password with given Key"() {
        given:
        String key = totpCommand.generateKey()
        int password = totpCommand.generatePassword(key)

        when:
        boolean result = totpCommand.validate(key, password)

        then:
        assert result == true
    }

    def "should validate password with given Key and return false"() {
        given:
        String key = totpCommand.generateKey()

        when:
        boolean result = totpCommand.validate(key, 111111)

        then:
        assert result == false
    }
}

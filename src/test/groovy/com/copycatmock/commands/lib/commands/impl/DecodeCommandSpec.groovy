package com.copycatmock.commands.lib.commands.impl

import spock.lang.Specification
import spock.lang.Unroll

class DecodeCommandSpec extends Specification {
    DecodeCommand decodeCommand

    def "setup"() {
        decodeCommand = new DecodeCommand()
    }

    @Unroll
    def "should encode string as base64 - #input - #expectedResult"() {
        when:
        String result = decodeCommand.fromBase64(input)

        then:
        assert result == expectedResult

        where:
        input              | expectedResult
        "dGVzdCBpbnB1dA==" | "test input"
        "YWJjZDEyMzQ="     | "abcd1234"
        "w6HDqcOtw7PDug==" | "áéíóú"
    }
}

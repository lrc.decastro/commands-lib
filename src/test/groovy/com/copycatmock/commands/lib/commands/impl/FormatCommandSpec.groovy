package com.copycatmock.commands.lib.commands.impl


import spock.lang.Specification
import spock.lang.Unroll

import java.time.LocalDate
import java.time.LocalDateTime

class FormatCommandSpec extends Specification {

    static final Locale DEFAULT_LOCALE = Locale.getDefault()

    DateTimeCommand dateAndTimeMock

    FormatCommand format

    def "setup"() {
        dateAndTimeMock = Mock(DateTimeCommand)

        format = new FormatCommand(dateAndTimeMock)
    }

    def "cleanupSpec"() {
        Locale.setDefault(DEFAULT_LOCALE)
    }

    @Unroll
    def "should format string - #stringFormat"() {
        when:
        String result = format.string(stringFormat, params.toArray())

        then:
        assert result == expectedResult

        where:
        stringFormat              | params        | expectedResult
        "number %d and number %d" | [1, 2]        | "number 1 and number 2"
        "city: %s"                | ["São Paulo"] | "city: São Paulo"
    }

    @Unroll
    def "should format string with left padding - #stringFormat"() {
        when:
        String result = format.stringLeftPad(stringFormat, length, padChar, params.toArray())

        then:
        assert result == expectedResult

        where:
        stringFormat | length | padChar | params   | expectedResult
        "%d.%d"      | 10     | "0"     | [1, 2]   | "00000001.2"
        "%s"         | 10     | " "     | ["test"] | "      test"
    }

    @Unroll
    def "should format string with right padding - #stringFormat"() {
        when:
        String result = format.stringRightPad(stringFormat, length, padChar, params.toArray())

        then:
        assert result == expectedResult

        where:
        stringFormat | length | padChar | params   | expectedResult
        "%d.%d"      | 10     | "0"     | [1, 2]   | "1.20000000"
        "%s"         | 10     | " "     | ["test"] | "test      "
    }

    @Unroll
    def "should format number - #numberFormat - #number"() {
        when:
        String result = format.number(numberFormat, number)

        then:
        assert result == expectedResult

        where:
        numberFormat | number         | expectedResult
        "#.00"       | 1              | "1.00"
        "#.00"       | 1              | "1.00"
        "#.00"       | 1.23           | "1.23"
        "#.000"      | 1.01           | "1.010"
        "#.00"       | 1.1            | "1.10"
        "#.00"       | "1.01"         | "1.01"
        "#.00"       | "123456789.01" | "123456789.01"
        ",###.00"    | "123456789.01" | "123,456,789.01"
    }

    @Unroll
    def "should format number with locale - #numberFormat - #number"() {
        when:
        String result = format.number(numberFormat, number, locale)

        then:
        assert result == expectedResult

        where:
        numberFormat | number         | locale  | expectedResult
        "#.00"       | 1.1            | "pt_BR" | "1,10"
        "#.00"       | "1.01"         | "pt_BR" | "1,01"
        "#.00"       | "123456789.01" | "pt_BR" | "123456789,01"
        ",###.00"    | "123456789.01" | "pt_BR" | "123.456.789,01"
    }

    @Unroll
    def "should format money - #number"() {
        given:
        Locale.setDefault(Locale.US)

        when:
        String result = format.money(number)

        then:
        assert result == expectedResult

        where:
        number          | expectedResult
        101             | '$101.00'
        1.1             | '$1.10'
        "1.01"          | '$1.01'
        "123456789.01"  | '$123,456,789.01'
        "123456789.053" | '$123,456,789.05'
    }

    @Unroll
    def "should format money with locale - #number"() {
        when:
        String result = format.money(number, locale)

        then:
        assert result == expectedResult

        where:
        number          | locale  | expectedResult
        101             | "pt_BR" | 'R$ 101,00'
        1.1             | "pt_BR" | 'R$ 1,10'
        "1.01"          | "pt_BR" | 'R$ 1,01'
        "123456789.01"  | "pt_BR" | 'R$ 123.456.789,01'
        "123456789.053" | "pt_BR" | 'R$ 123.456.789,05'
    }

    @Unroll
    def "should format now - #dateFormat"() {
        given:
        LocalDateTime localDateTime = LocalDate.of(year, month, day).atTime(hour, minute, second)

        when:
        String result = format.now(dateFormat)

        then:
        1 * dateAndTimeMock.now() >> localDateTime
        assert result == expectedResult

        where:
        dateFormat | year | month | day | hour | minute | second | expectedResult
        "HHmmss"   | 2020 | 01    | 22  | 13   | 20     | 22     | "132022"
        "MMdd"     | 2020 | 01    | 22  | 13   | 20     | 22     | "0122"
        "MMdd"     | 2020 | 01    | 22  | 13   | 20     | 22     | "0122"
    }

    @Unroll
    def "should format now with zoneId - #dateFormat"() {
        given:
        String zoneId = "Europe/Berlin"
        LocalDateTime localDateTime = LocalDate.of(year, month, day).atTime(hour, minute, second)

        when:
        String result = format.now(dateFormat, zoneId)

        then:
        1 * dateAndTimeMock.now(zoneId) >> localDateTime
        assert result == expectedResult

        where:
        dateFormat                 | year | month | day | hour | minute | second | expectedResult
        "HHmmss"                   | 2020 | 01    | 22  | 13   | 20     | 22     | "132022"
        "MMdd"                     | 2020 | 01    | 22  | 13   | 20     | 22     | "0122"
        "YYYY-MM-dd'T'HH:mm:ss'Z'" | 2020 | 9     | 23  | 13   | 54     | 0      | "2020-09-23T13:54:00Z"
    }

    @Unroll
    def "should format received localDateTime - #dateFormat"() {
        given:
        LocalDateTime localDateTime = LocalDate.of(year, month, day).atTime(hour, minute, second)

        when:
        String result = format.localDateTime(dateFormat, localDateTime)

        then:
        assert result == expectedResult

        where:
        dateFormat | year | month | day | hour | minute | second | expectedResult
        "HHmmss"   | 2020 | 01    | 22  | 13   | 20     | 22     | "132022"
        "MMdd"     | 2020 | 01    | 22  | 13   | 20     | 22     | "0122"
        "MMdd"     | 2020 | 01    | 22  | 13   | 20     | 22     | "0122"
    }

}

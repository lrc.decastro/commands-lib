package com.copycatmock.commands.lib.commands.impl

import spock.lang.Specification
import spock.lang.Unroll

import java.time.Clock
import java.time.LocalDate
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

class DateCommandSpec extends Specification {

    private static final String SP_ZONE_ID = "America/Sao_Paulo"
    private static final ZoneId SP_ZONE = ZoneId.of(SP_ZONE_ID)
    private static final ZonedDateTime SP_ZONE_DATE_TIME = LocalDate.of(2020, 9, 23).atTime(13, 54, 0).atZone(SP_ZONE)
    private static final long SP_TIMESTAMP = SP_ZONE_DATE_TIME.toInstant().toEpochMilli()

    private static final String BERLIN_ZONE_ID = "Europe/Berlin"
    private static final ZonedDateTime BERLIN_ZONE_DATE_TIME = LocalDate.of(2020, 9, 23).atTime(18, 54, 0).atZone(SP_ZONE)

    DateCommand dateCmd

    def "setup"() {
        Clock clockStub = Clock.fixed(SP_ZONE_DATE_TIME.toInstant(), SP_ZONE)
        dateCmd = new DateCommand(clockStub)
    }

    def "should get current timestamp"() {
        when:
        long result = dateCmd.timestamp()

        then:
        assert result == SP_TIMESTAMP
    }

    def "should format now"() {
        when:
        LocalDate result = dateCmd.now()

        then:
        assert result == SP_ZONE_DATE_TIME.toLocalDate()
    }


    def "should format now - with zoneId"() {
        when:
        LocalDate result = dateCmd.now(BERLIN_ZONE_ID)

        then:
        assert result == BERLIN_ZONE_DATE_TIME.toLocalDate()
    }

    def "should parse date"() {
        given:
        String date = "2021-01-22"
        LocalDate expectedDate = LocalDate.of(2021, 01, 22)

        when:
        LocalDate result = dateCmd.parse(date)

        then:
        assert result == expectedDate
    }

    def "should parse date with format"() {
        given:
        String date = "2021-01-22"
        String format = "yyyy-MM-dd"
        LocalDate expectedDate = LocalDate.of(2021, 01, 22)

        when:
        LocalDate result = dateCmd.parse(date, format)

        then:
        assert result == expectedDate
    }

    def "should parse date with formatter"() {
        given:
        String date = "2021-01-22"
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        LocalDate expectedDate = LocalDate.of(2021, 01, 22)

        when:
        LocalDate result = dateCmd.parse(date, formatter)

        then:
        assert result == expectedDate
    }

    @Unroll
    def "should check if one date is before than another date"() {
        when:
        boolean result = dateCmd.isBefore(dateA, formatA, dateB, formatB)

        then:
        assert result == expectedResult

        where:
        dateA        | formatA      | dateB        | formatB      | expectedResult
        '2021-05-29' | 'yyyy-MM-dd' | '17/05/2021' | 'dd/MM/yyyy' | false
        '2021-05-29' | 'yyyy-MM-dd' | '29/05/2021' | 'dd/MM/yyyy' | false
        '17/05/2021' | 'dd/MM/yyyy' | '2021-05-29' | 'yyyy-MM-dd' | true
    }
}

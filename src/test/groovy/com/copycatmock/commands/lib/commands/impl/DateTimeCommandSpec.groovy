package com.copycatmock.commands.lib.commands.impl


import spock.lang.Specification
import spock.lang.Unroll

import java.time.*
import java.time.format.DateTimeFormatter

class DateTimeCommandSpec extends Specification {

    private static final String SP_ZONE_ID = "America/Sao_Paulo"
    private static final ZoneId SP_ZONE = ZoneId.of(SP_ZONE_ID)
    private static final ZonedDateTime SP_ZONE_DATE_TIME = LocalDate.of(2020, 9, 23).atTime(13, 54, 0).atZone(SP_ZONE)
    private static final long SP_TIMESTAMP = SP_ZONE_DATE_TIME.toInstant().toEpochMilli()

    private static final String BERLIN_ZONE_ID = "Europe/Berlin"
    private static final ZonedDateTime BERLIN_ZONE_DATE_TIME = LocalDate.of(2020, 9, 23).atTime(18, 54, 0).atZone(SP_ZONE)

    DateTimeCommand dateAndTime

    def "setup"() {
        Clock clockStub = Clock.fixed(SP_ZONE_DATE_TIME.toInstant(), SP_ZONE)
        dateAndTime = new DateTimeCommand(clockStub)
    }

    def "should get current timestamp"() {
        when:
        long result = dateAndTime.timestamp()

        then:
        assert result == SP_TIMESTAMP
    }

    def "should format now"() {
        when:
        LocalDateTime result = dateAndTime.now()

        then:
        assert result == SP_ZONE_DATE_TIME.toLocalDateTime()
    }


    def "should format now - with zoneId"() {
        when:
        LocalDateTime result = dateAndTime.now(BERLIN_ZONE_ID)

        then:
        assert result == BERLIN_ZONE_DATE_TIME.toLocalDateTime()
    }

    def "should parse dateTime"() {
        given:
        String dateTime = "2021-01-22T01:45:25"
        LocalDateTime expectedDateTime = LocalDateTime.of(2021, 01, 22, 01, 45, 25)

        when:
        LocalDateTime result = dateAndTime.parse(dateTime)

        then:
        assert result == expectedDateTime
    }

    def "should parse dateTime with format"() {
        given:
        String dateTime = "2021-01-22T01:45:25Z"
        String format = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        LocalDateTime expectedDateTime = LocalDateTime.of(2021, 01, 22, 01, 45, 25)

        when:
        LocalDateTime result = dateAndTime.parse(dateTime, format)

        then:
        assert result == expectedDateTime
    }

    def "should parse dateTime with formatter"() {
        given:
        String dateTime = "2021-01-22T01:45:25Z"
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")
        LocalDateTime expectedDateTime = LocalDateTime.of(2021, 01, 22, 01, 45, 25)

        when:
        LocalDateTime result = dateAndTime.parse(dateTime, formatter)

        then:
        assert result == expectedDateTime
    }

    @Unroll
    def "should check if one date is before than another date"() {
        when:
        boolean result = dateAndTime.isBefore(dateA, formatA, dateB, formatB)

        then:
        assert result == expectedResult

        where:
        dateA                  | formatA                    | dateB                  | formatB                    | expectedResult
        '2021-05-29T01:45:25Z' | "yyyy-MM-dd'T'HH:mm:ss'Z'" | '17/05/2021T01:45:25Z' | "dd/MM/yyyy'T'HH:mm:ss'Z'" | false
        '2021-05-29T01:45:25Z' | "yyyy-MM-dd'T'HH:mm:ss'Z'" | '29/05/2021T01:45:25Z' | "dd/MM/yyyy'T'HH:mm:ss'Z'" | false
        '2021-05-29T01:45:25Z' | "yyyy-MM-dd'T'HH:mm:ss'Z'" | '29/05/2021T02:45:25Z' | "dd/MM/yyyy'T'HH:mm:ss'Z'" | true
        '17/05/2021T01:45:25Z' | "dd/MM/yyyy'T'HH:mm:ss'Z'" | '2021-05-29T01:45:25Z' | "yyyy-MM-dd'T'HH:mm:ss'Z'" | true
    }
}

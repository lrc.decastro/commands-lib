package com.copycatmock.commands.lib.commands.impl


import com.copycatmock.commands.lib.provided.DatabaseSupplier
import spock.lang.Specification

class IdCommandSpec extends Specification {

    RandomCommand randomGeneratorMock
    DatabaseSupplier dbSupplierMock

    IdCommand idGenerator

    def "setup"() {
        randomGeneratorMock = Mock(RandomCommand)
        dbSupplierMock = Mock(DatabaseSupplier)

        idGenerator = new IdCommand(randomGeneratorMock, dbSupplierMock)
    }

    def "should generate sequence id"() {
        given:
        String collectionName = "testCollection"
        Long expectedResult = 1234L

        when:
        Long result = idGenerator.seqId(collectionName)

        then:
        1 * dbSupplierMock.getNextId(collectionName) >> expectedResult

        assert result == expectedResult
    }

    def "should generate random UUID"() {
        when:
        String result = idGenerator.uuid()

        then:
        assert result != null
        assert result.contains("-")
    }

    def "should generate random UUID without dashes"() {
        when:
        String result = idGenerator.uuidWithoutDashes()

        then:
        assert result != null
        assert !result.contains("-")
    }

    def "should generate random number"() {
        given:
        int length = 2
        int expectedResult = 10

        when:
        long result = idGenerator.numericId(length)

        then:
        1 * randomGeneratorMock.nextLong(length) >> expectedResult
        assert result == expectedResult
    }

    def "should generate random string"() {
        given:
        int length = 3
        String expectedResult = "abc"

        when:
        String result = idGenerator.stringId(length)

        then:
        1 * randomGeneratorMock.nextString(length) >> expectedResult
        assert result == expectedResult
    }

}

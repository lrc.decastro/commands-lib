package com.copycatmock.commands.lib.commands.impl

import spock.lang.Specification
import spock.lang.Unroll

class EncodeCommandSpec extends Specification {

    EncodeCommand encodeCommand

    def "setup"() {
        encodeCommand = new EncodeCommand()
    }


    @Unroll
    def "should encode string as base64 - #input - #expectedResult"() {
        when:
        String result = encodeCommand.toBase64(input)

        then:
        assert result == expectedResult

        where:
        input        | expectedResult
        "test input" | "dGVzdCBpbnB1dA=="
        "abcd1234"   | "YWJjZDEyMzQ="
        "áéíóú"      | "w6HDqcOtw7PDug=="
    }
}

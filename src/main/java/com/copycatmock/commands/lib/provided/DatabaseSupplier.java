package com.copycatmock.commands.lib.provided;

import java.util.List;
import java.util.Map;

import com.copycatmock.commands.lib.commands.Command;

public interface DatabaseSupplier extends Command {

  boolean exists(String collectionName, Object id);

  Object get(String collectionName, Object id);

  Object getBy(String collectionName, Map<String, Object> fieldFilters);

  List<?> list(String collectionName);

  List<?> listBy(String collectionName, Map<String, Object> fieldFilters);

  void save(String collectionName, Object value);

  void save(String collectionName, Object id, Object value);

  Object update(String collectionName, Object id, Object value);

  void delete(String collectionName, Object id);

  Long getNextId(String collectionName);

  default String getCommandKey() {
    return "db";
  }
}

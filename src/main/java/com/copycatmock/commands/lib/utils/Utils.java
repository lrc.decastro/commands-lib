package com.copycatmock.commands.lib.utils;

public class Utils {

  public static int random(int n) {
    return (int) (Math.random() * n);
  }

  public static int mod(int dividend, int divider) {
    return (int) Math.round(dividend - (Math.floor(dividend / divider) * divider));
  }
}

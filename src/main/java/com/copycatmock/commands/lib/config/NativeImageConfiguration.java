package com.copycatmock.commands.lib.config;

import nativeimage.Reflection;

@Reflection(scanPackage = "com.copycatmock", constructors = true, declaredConstructors = true, declaredFields = true, declaredMethods = true, publicConstructors = true, publicFields = true, publicMethods = true)
public class NativeImageConfiguration {
}

package com.copycatmock.commands.lib.commands.impl.country.br;

import com.copycatmock.commands.lib.commands.impl.FormatCommand;
import com.copycatmock.commands.lib.commands.impl.country.CountryCommand;
import com.copycatmock.commands.lib.commands.impl.country.br.components.CNPJ;
import com.copycatmock.commands.lib.commands.impl.country.br.components.CPF;
import com.copycatmock.commands.lib.commands.impl.faker.FakerCommand;

public class BrCommand extends CountryCommand {

  private static final String ZONE_ID = "America/Sao_Paulo";
  private static final String LOCALE_NAME = "pt_BR";

  public BrCommand(FormatCommand formatCommand, FakerCommand fakerCommand) {
    super(formatCommand, fakerCommand);
  }

  public CPF cpf() {
    return new CPF();
  }

  public CNPJ cnpj() {
    return new CNPJ();
  }

  @Override
  public String getCommandKey() {
    return "br";
  }

  @Override
  public String getZoneId() {
    return ZONE_ID;
  }

  @Override
  public String getLocaleName() {
    return LOCALE_NAME;
  }
}

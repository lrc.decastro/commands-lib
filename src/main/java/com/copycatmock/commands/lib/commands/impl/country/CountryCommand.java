package com.copycatmock.commands.lib.commands.impl.country;

import com.copycatmock.commands.lib.commands.Command;
import com.copycatmock.commands.lib.commands.impl.FormatCommand;
import com.copycatmock.commands.lib.commands.impl.faker.FakerCommand;
import com.github.javafaker.Faker;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class CountryCommand implements Command {

  protected final FormatCommand formatCmd;
  protected final FakerCommand fakerCmd;

  public abstract String getZoneId();

  public abstract String getLocaleName();

  public String now(String format) {
    return formatCmd.now(format, getZoneId());
  }

  public String money(Object number) {
    return formatCmd.money(number, getLocaleName());
  }

  public Faker faker() {
    return fakerCmd.withLocale(getLocaleName());
  }

}

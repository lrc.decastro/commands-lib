package com.copycatmock.commands.lib.commands.impl.faker;

import java.util.Locale;

import com.copycatmock.commands.lib.commands.Command;
import org.apache.commons.lang3.LocaleUtils;

import com.github.javafaker.Faker;

public class FakerCommand extends Faker implements Command {

  public Faker withLocale(String localeName) {
    Locale locale = LocaleUtils.toLocale(localeName);
    return new Faker(locale);
  }

  @Override
  public String getCommandKey() {
    return "faker";
  }
}

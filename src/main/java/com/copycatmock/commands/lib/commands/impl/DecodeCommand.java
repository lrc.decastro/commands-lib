package com.copycatmock.commands.lib.commands.impl;

import java.util.Base64;

import com.copycatmock.commands.lib.commands.Command;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class DecodeCommand implements Command {

  public byte[] fromBase64Bytes(String value) {
    return Base64.getDecoder().decode(value);
  }

  public String fromBase64(String value) {
    return new String(fromBase64Bytes(value));
  }

  @Override
  public String getCommandKey() {
    return "decode";
  }
}

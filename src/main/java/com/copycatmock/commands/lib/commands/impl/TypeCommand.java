package com.copycatmock.commands.lib.commands.impl;

import com.copycatmock.commands.lib.commands.Command;
import lombok.RequiredArgsConstructor;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

@RequiredArgsConstructor
public class TypeCommand implements Command {

  public boolean isInstance(String className, Object obj) throws ClassNotFoundException {
    return isInstance(Class.forName(className), obj);
  }

  public boolean isInstance(Class<?> objectClass, Object obj) {
    return objectClass.isInstance(obj);
  }

  public boolean isInteger(Object obj) {
    return obj instanceof Integer;
  }

  public boolean isNumber(Object obj) {
    return obj instanceof Number;
  }

  public boolean isString(Object obj) {
    return obj instanceof String;
  }

  public boolean isBoolean(Object obj) {
    return obj instanceof Boolean;
  }

  public boolean isCollection(Object obj) {
    return obj instanceof Collection;
  }

  public boolean isList(Object obj) {
    return obj instanceof List;
  }

  public boolean isSet(Object obj) {
    return obj instanceof Set;
  }

  public boolean isMap(Object obj) {
    return obj instanceof Map;
  }

  @Override
  public String getCommandKey() {
    return "type";
  }
}

package com.copycatmock.commands.lib.commands.impl.totp.components;

import java.util.Base64;

import javax.crypto.SecretKey;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class StubKey implements SecretKey {

  private static final String DEFAULT_ALGORITHM = "HmacSHA1";
  private static final String DEFAULT_ENCODED_VALUE = "1+1+y4iQjEqGz79dW9Kox7kaVQs=";

  private final String format = "RAW";
  private final String algorithm;
  private final byte[] encoded;

  public StubKey() {
    this(DEFAULT_ALGORITHM, DEFAULT_ENCODED_VALUE);
  }

  public StubKey(String algorithm, String value) {
    this(algorithm, Base64.getDecoder().decode(value));
  }

}

package com.copycatmock.commands.lib.commands.impl;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.StringUtils;

import com.copycatmock.commands.lib.commands.Command;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FormatCommand implements Command {

  private final DateTimeCommand dateAndTime;

  public String string(String format, Object... params) {
    return String.format(format, params);
  }

  public String stringLeftPad(String string, int length, String padStr) {
    return StringUtils.leftPad(string, length, padStr);
  }

  public String stringLeftPad(String format, int length, String padStr, Object... args) {
    String formatted = String.format(format, args);
    return stringLeftPad(formatted, length, padStr);
  }

  public String stringRightPad(String string, int length, String padStr) {
    return StringUtils.rightPad(string, length, padStr);
  }

  public String stringRightPad(String format, int length, String padStr, Object... args) {
    String formatted = String.format(format, args);
    return stringRightPad(formatted, length, padStr);
  }

  public String number(String format, Object number) {
    return number(format, number, Locale.getDefault());
  }

  public String number(String format, Object number, String localeName) {
    return number(format, number, LocaleUtils.toLocale(localeName));
  }

  private String number(String format, Object numberObj, Locale locale) {
    Number number = numberObj instanceof Number ? (Number) numberObj : Double.valueOf(numberObj.toString());
    DecimalFormat decimalFormat = new DecimalFormat(format, new DecimalFormatSymbols(locale));
    return decimalFormat.format(number);
  }

  public String money(Object number) {
    return money(number, Locale.getDefault());
  }

  public String money(Object number, String localeName) {
    return money(number, LocaleUtils.toLocale(localeName));
  }

  private String money(Object numberObj, Locale locale) {
    Number number = numberObj instanceof Number ? (Number) numberObj : Double.valueOf(numberObj.toString());
    NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(locale);
    return currencyFormat.format(number);
  }

  public String now(String format) {
    return localDateTime(format, dateAndTime.now());
  }

  public String now(String format, String zoneId) {
    return localDateTime(format, dateAndTime.now(zoneId));
  }

  public String localDateTime(String format, LocalDateTime value) {
    DateTimeFormatter formatter = dateTimeFormatter(format);
    return value.format(formatter);
  }

  public DateTimeFormatter dateTimeFormatter(String format) {
    return DateTimeFormatter.ofPattern(format);
  }

  @Override
  public String getCommandKey() {
    return "format";
  }
}

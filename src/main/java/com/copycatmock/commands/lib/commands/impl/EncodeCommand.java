package com.copycatmock.commands.lib.commands.impl;

import java.util.Base64;

import com.copycatmock.commands.lib.commands.Command;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class EncodeCommand implements Command {

  public String toBase64(String value) {
    return Base64.getEncoder().encodeToString(value.getBytes());
  }

  @Override
  public String getCommandKey() {
    return "encode";
  }
}

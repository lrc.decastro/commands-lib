package com.copycatmock.commands.lib.commands.impl.country.br.components;

import com.copycatmock.commands.lib.utils.Utils;
import com.fasterxml.jackson.annotation.JsonValue;

import lombok.Getter;

@Getter
public class CNPJ {

  private static final int NUMBER_OF_RANDOM_DIGITS = 9;

  private final int n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, d1, d2;

  public CNPJ() {
    this.n1 = Utils.random(NUMBER_OF_RANDOM_DIGITS);
    this.n2 = Utils.random(NUMBER_OF_RANDOM_DIGITS);
    this.n3 = Utils.random(NUMBER_OF_RANDOM_DIGITS);
    this.n4 = Utils.random(NUMBER_OF_RANDOM_DIGITS);
    this.n5 = Utils.random(NUMBER_OF_RANDOM_DIGITS);
    this.n6 = Utils.random(NUMBER_OF_RANDOM_DIGITS);
    this.n7 = Utils.random(NUMBER_OF_RANDOM_DIGITS);
    this.n8 = Utils.random(NUMBER_OF_RANDOM_DIGITS);
    this.n9 = 0;
    this.n10 = 0;
    this.n11 = 0;
    this.n12 = 1;

    this.d1 = calculateD1();
    this.d2 = calculateD2();
  }

  private int calculateD1() {

    int d1 = n12 * 2 + n11 * 3 + n10 * 4 + n9 * 5 + n8 * 6 + n7 * 7 + n6 * 8 + n5 * 9 + n4 * 2 + n3 * 3 + n2 * 4 + n1 * 5;

    d1 = 11 - (Utils.mod(d1, 11));
    return d1 >= 10 ? 0 : d1;
  }

  private int calculateD2() {
    int d2 = d1 * 2 + n12 * 3 + n11 * 4 + n10 * 5 + n9 * 6 + n8 * 7 + n7 * 8 + n6 * 9 + n5 * 2 + n4 * 3 + n3 * 4 + n2 * 5 + n1 * 6;

    d2 = 11 - (Utils.mod(d2, 11));
    return d2 >= 10 ? 0 : d2;
  }

  @JsonValue
  public String toString() {
    return "" + n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8 + n9 + n10 + n11 + n12 + d1 + d2;
  }

  public String format() {
    return "" + n1 + n2 + "." + n3 + n4 + n5 + "." + n6 + n7 + n8 + "/" + n9 + n10 + n11 + n12 + "-" + d1 + d2;
  }
}

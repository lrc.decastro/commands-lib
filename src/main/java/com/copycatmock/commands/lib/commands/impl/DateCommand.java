package com.copycatmock.commands.lib.commands.impl;

import java.time.Clock;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import com.copycatmock.commands.lib.commands.Command;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class DateCommand implements Command {

  private final Clock clock;

  public long timestamp() {
    return clock.millis();
  }

  public LocalDate now() {
    return LocalDate.now(clock);
  }

  public LocalDate now(String zoneId) {
    Clock zoneClock = getClock(zoneId);
    return LocalDate.now(zoneClock);
  }

  public LocalDate parse(String date) {
    return LocalDate.parse(date);
  }

  public LocalDate parse(String date, String format) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
    return LocalDate.parse(date, formatter);
  }

  public LocalDate parse(String date, DateTimeFormatter formatter) {
    return LocalDate.parse(date, formatter);
  }

  public boolean isBefore(String dateA, String formatA, String dateB, String formatB) {
    LocalDate localDateA = parse(dateA, formatA);
    LocalDate localDateB = parse(dateB, formatB);

    return localDateA.isBefore(localDateB);
  }

  private Clock getClock(String zoneId) {
    return clock.withZone(ZoneId.of(zoneId));
  }

  @Override
  public String getCommandKey() {
    return "date";
  }
}

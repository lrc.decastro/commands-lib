package com.copycatmock.commands.lib.commands.impl.totp;

import java.time.Clock;
import java.util.Base64;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import com.copycatmock.commands.lib.commands.Command;
import com.copycatmock.commands.lib.commands.impl.totp.components.StubKey;
import com.eatthepath.otp.TimeBasedOneTimePasswordGenerator;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

@RequiredArgsConstructor
public class TotpCommand implements Command {

  private static final int DEFAULT_KEY_LENGTH = 160;

  private final Clock clock;
  private final TimeBasedOneTimePasswordGenerator totp;
  private final int keyLength;

  public TotpCommand(Clock clock) {
    this(clock, getDefaultTimeBasedOneTimePasswordGenerator(), DEFAULT_KEY_LENGTH);
  }

  @SneakyThrows
  public int generatePassword() {
    StubKey key = new StubKey();
    return totp.generateOneTimePassword(key, clock.instant());
  }

  @SneakyThrows
  public int generatePassword(String encodedKey) {
    StubKey key = new StubKey(totp.getAlgorithm(), encodedKey);
    return totp.generateOneTimePassword(key, clock.instant());
  }

  public boolean validate(int password) {
    int generatedPassword = generatePassword();
    return password == generatedPassword;
  }

  public boolean validate(String encodedKey, int password) {
    int generatedPassword = generatePassword(encodedKey);
    return password == generatedPassword;
  }

  @SneakyThrows
  public String generateKey() {
    KeyGenerator keyGenerator = KeyGenerator.getInstance(totp.getAlgorithm());
    keyGenerator.init(keyLength);
    SecretKey key = keyGenerator.generateKey();
    return Base64.getEncoder().encodeToString(key.getEncoded());
  }

  @Override
  public String getCommandKey() {
    return "totp";
  }

  @SneakyThrows
  private static TimeBasedOneTimePasswordGenerator getDefaultTimeBasedOneTimePasswordGenerator() {
    return new TimeBasedOneTimePasswordGenerator();
  }
}

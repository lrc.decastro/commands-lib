package com.copycatmock.commands.lib.commands.impl.country.br.components;

import static com.copycatmock.commands.lib.utils.Utils.mod;
import static com.copycatmock.commands.lib.utils.Utils.random;

import com.fasterxml.jackson.annotation.JsonValue;

import lombok.Getter;

@Getter
public class CPF {

  private static final int NUMBER_OF_RANDOM_DIGITS = 9;

  private final int n1, n2, n3, n4, n5, n6, n7, n8, n9, d1, d2;

  public CPF() {
    this.n1 = random(NUMBER_OF_RANDOM_DIGITS);
    this.n2 = random(NUMBER_OF_RANDOM_DIGITS);
    this.n3 = random(NUMBER_OF_RANDOM_DIGITS);
    this.n4 = random(NUMBER_OF_RANDOM_DIGITS);
    this.n5 = random(NUMBER_OF_RANDOM_DIGITS);
    this.n6 = random(NUMBER_OF_RANDOM_DIGITS);
    this.n7 = random(NUMBER_OF_RANDOM_DIGITS);
    this.n8 = random(NUMBER_OF_RANDOM_DIGITS);
    this.n9 = random(NUMBER_OF_RANDOM_DIGITS);

    this.d1 = calculateD1();
    this.d2 = calculateD2();
  }

  private int calculateD1() {
    int d1 = n9 * 2 + n8 * 3 + n7 * 4 + n6 * 5 + n5 * 6 + n4 * 7 + n3 * 8 + n2 * 9 + n1 * 10;

    d1 = 11 - (mod(d1, 11));
    return d1 >= 10 ? 0 : d1;
  }

  private int calculateD2() {
    int d2 = d1 * 2 + n9 * 3 + n8 * 4 + n7 * 5 + n6 * 6 + n5 * 7 + n4 * 8 + n3 * 9 + n2 * 10 + n1 * 11;

    d2 = 11 - (mod(d2, 11));
    return d2 >= 10 ? 0 : d2;
  }

  @JsonValue
  public String toString() {
    return "" + n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8 + n9 + d1 + d2;
  }

  public String format() {
    return "" + n1 + n2 + n3 + '.' + n4 + n5 + n6 + '.' + n7 + n8 + n9 + '-' + d1 + d2;
  }
}

package com.copycatmock.commands.lib.commands.impl;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import com.copycatmock.commands.lib.commands.Command;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class DateTimeCommand implements Command {

  private final Clock clock;

  public long timestamp() {
    return clock.millis();
  }

  public LocalDateTime now() {
    return LocalDateTime.now(clock);
  }

  public LocalDateTime now(String zoneId) {
    Clock zoneClock = getClock(zoneId);
    return LocalDateTime.now(zoneClock);
  }

  public LocalDateTime parse(String dateTime) {
    return LocalDateTime.parse(dateTime);
  }

  public LocalDateTime parse(String dateTime, String format) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
    return LocalDateTime.parse(dateTime, formatter);
  }

  public LocalDateTime parse(String dateTime, DateTimeFormatter formatter) {
    return LocalDateTime.parse(dateTime, formatter);
  }

  public boolean isBefore(String dateTimeA, String formatA, String dateTimeB, String formatB) {
    LocalDateTime localDateTimeA = parse(dateTimeA, formatA);
    LocalDateTime localDateTimeB = parse(dateTimeB, formatB);

    return localDateTimeA.isBefore(localDateTimeB);
  }

  private Clock getClock(String zoneId) {
    return clock.withZone(ZoneId.of(zoneId));
  }

  @Override
  public String getCommandKey() {
    return "dateTime";
  }
}

package com.copycatmock.commands.lib.commands.impl;

import java.util.UUID;

import com.copycatmock.commands.lib.commands.Command;
import com.copycatmock.commands.lib.provided.DatabaseSupplier;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class IdCommand implements Command {

  private final RandomCommand random;
  private final DatabaseSupplier dbSupplier;

  public Long seqId(String collectionName) {
    return dbSupplier.getNextId(collectionName);
  }

  public String uuid() {
    return UUID.randomUUID().toString();
  }

  public String uuidWithoutDashes() {
    return uuid().replace("-", "");
  }

  public long numericId(int length) {
    return random.nextLong(length);
  }

  public String stringId(int length) {
    return random.nextString(length);
  }

  @Override
  public String getCommandKey() {
    return "id";
  }
}

package com.copycatmock.commands.lib.commands.impl;

import java.math.BigDecimal;
import java.math.BigInteger;

import com.copycatmock.commands.lib.commands.Command;

public class ConvertCommand implements Command {

  public Integer toInteger(Object obj) {
    return obj != null ? Integer.valueOf(obj.toString()) : null;
  }

  public Long toLong(Object obj) {
    return obj != null ? Long.valueOf(obj.toString()) : null;
  }

  public Float toFloat(Object obj) {
    return obj != null ? Float.valueOf(obj.toString()) : null;
  }

  public Double toDouble(Object obj) {
    return obj != null ? Double.valueOf(obj.toString()) : null;
  }

  public BigInteger toBigInteger(Object obj) {
    return obj != null ? new BigInteger(obj.toString()) : null;
  }

  public BigDecimal toBigDecimal(Object obj) {
    return obj != null ? new BigDecimal(obj.toString()) : null;
  }

  public String toString(Object obj) {
    return obj != null ? obj.toString() : null;
  }

  @Override
  public String getCommandKey() {
    return "convert";
  }
}

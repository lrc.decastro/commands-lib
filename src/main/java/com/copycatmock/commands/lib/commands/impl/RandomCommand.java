package com.copycatmock.commands.lib.commands.impl;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.util.Random;

import com.copycatmock.commands.lib.commands.Command;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class RandomCommand implements Command {

  private static final char[] ALPHA_NUMERIC_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890".toCharArray();
  private static final char[] NUMERIC_CHARS = "1234567890".toCharArray();
  private static final int MAX_NUMBER_SIZE = 50;
  private static final int MAX_STRING_SIZE = 100;

  private final Random r;

  public int nextInt() {
    return Math.abs(r.nextInt());
  }

  public int nextInt(int length) {
    return (int) nextDouble(length);
  }

  public long nextLong() {
    return Math.abs(r.nextLong());
  }

  public long nextLong(int length) {
    return (long) nextDouble(length);
  }

  public float nextFloat() {
    return Math.abs(r.nextFloat());
  }

  public float nextFloat(int length) {
    return (long) nextDouble(length);
  }

  public double nextDouble() {
    return Math.abs(r.nextDouble());
  }

  public double nextDouble(int length) {
    length = length < 1 ? 1 : Math.min(length, MAX_NUMBER_SIZE);

    double min = Math.pow(10, length - 1);
    double max = Math.pow(10, length);

    return between(min, max);
  }

  public String nextString(int length) {
    return nextString(length, ALPHA_NUMERIC_CHARS);
  }

  public String nextNumericString(int length) {
    String numericString = nextString(length, NUMERIC_CHARS);
    return isNotBlank(numericString) ? numericString : "0";
  }

  public double between(double min, double max) {
    return Math.floor(r.nextDouble() * (max - min)) + min;
  }

  public String nextString(int length, char[] chars) {
    length = length < 0 ? 0 : Math.min(length, MAX_STRING_SIZE);
    char[] buf = new char[length];
    for (int idx = 0; idx < buf.length; ++idx) {
      buf[idx] = chars[r.nextInt(chars.length)];
    }
    return new String(buf);
  }

  @Override
  public String getCommandKey() {
    return "random";
  }
}

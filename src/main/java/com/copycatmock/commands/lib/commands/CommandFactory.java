package com.copycatmock.commands.lib.commands;

import com.copycatmock.commands.lib.commands.impl.*;
import com.copycatmock.commands.lib.commands.impl.country.br.BrCommand;
import com.copycatmock.commands.lib.commands.impl.faker.FakerCommand;
import com.copycatmock.commands.lib.commands.impl.totp.TotpCommand;
import com.copycatmock.commands.lib.provided.DatabaseSupplier;

import java.security.SecureRandom;
import java.time.Clock;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class CommandFactory {

  private final Map<String, Command> commands = new HashMap<>();

  public CommandFactory(DatabaseSupplier dbSupplier) {
    this(dbSupplier, new SecureRandom(), Clock.systemDefaultZone());
  }

  public CommandFactory(DatabaseSupplier dbSupplier, Random random, Clock clock) {
    FakerCommand fakerCmd = new FakerCommand();

    DateCommand dateCmd = new DateCommand(clock);
    DateTimeCommand dateTimeCmd = new DateTimeCommand(clock);
    RandomCommand randomCmd = new RandomCommand(random);
    FormatCommand formatCmd = new FormatCommand(dateTimeCmd);

    addCommand(dbSupplier);
    addCommand(dateCmd);
    addCommand(dateTimeCmd);
    addCommand(randomCmd);
    addCommand(formatCmd);
    addCommand(fakerCmd);

    addCommand(new IdCommand(randomCmd, dbSupplier));
    addCommand(new ConvertCommand());
    addCommand(new TotpCommand(clock));
    addCommand(new TypeCommand());
    addCommand(new ListCommand());

    addCommand(new EncodeCommand());
    addCommand(new DecodeCommand());

    addCommand(new BrCommand(formatCmd, fakerCmd));
  }

  public Map<String, Command> getCommands() {
    return Collections.unmodifiableMap(commands);
  }

  private void addCommand(Command command) {
    commands.put(command.getCommandKey(), command);
  }

}

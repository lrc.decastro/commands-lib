package com.copycatmock.commands.lib.commands;

public interface Command {

  String getCommandKey();
}

package com.copycatmock.commands.lib.commands.impl;

import com.copycatmock.commands.lib.commands.Command;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ListCommand implements Command {

  public boolean isEmpty(List<?> list) {
    return list == null || list.isEmpty();
  }

  public <T> List<T> filter(List<T> list, Predicate<T> predicate) {
    return list.stream().filter(predicate).collect(Collectors.toList());
  }

  @Override
  public String getCommandKey() {
    return "list";
  }
}
